import tkinter as tk
import cv2

class Controller:

    threshold = 100
    iteration = 2

    def __init__(self, view, window, capinfo, canvas):

        self.view_class = view

        self.window = window
        self.width = capinfo.get(cv2.CAP_PROP_FRAME_WIDTH)
        self.height = capinfo.get(cv2.CAP_PROP_FRAME_HEIGHT)
        # self.width = 1920
        # self.height = 1080
        
        # Closeボタン
        self.close_btn = tk.Button(window, text="Close")
        self.close_btn.pack()
        self.close_btn.configure(command=self.ctrldestructor)
        # Rollチェックボタン
        # self.roll_b = tk.Label(window).grid(row=3, sticky="e")
        self.roll_btn = tk.Checkbutton(window, text="roll 180deg", command =self.ctrsirection)
        self.roll_btn.pack()

        # スケール
        self.scale1 = tk.Scale(self.window, label='Threshold', orient='h', from_=-255, to=255,length=self.width, command=self.ctrlthreshold)
        self.scale1.set(self.threshold)
        self.scale1.pack()
        self.scale2 = tk.Scale(self.window, label='iteration', orient='h', from_=0, to=4,length=self.width, command=self.ctrliteration)
        self.scale2.set(self.iteration)
        self.scale2.pack()

        # マウスイベントの登録
        canvas.tag_bind("main", "<ButtonPress-1>", self.Clicked)
        canvas.tag_bind("main", "<B1-Motion>", self.Released)

    def ctrldestructor(self):
        self.view_class.destructor()

    def ctrlthreshold(self, n):
        self.threshold = self.scale1.get()
        self.view_class.setthreshold(self.threshold)
        # print(self.threshold)

    def ctrliteration(self, n):
        self.iteration = self.scale2.get()
        self.view_class.setiteration(self.iteration)
        # print(self.threshold)

    def Clicked(self, event):
        self.startX = event.x
        self.startY = event.y 
        self.endX = event.x
        self.endY = event.y

    def Released(self, event):
        self.endX = event.x
        self.endY = event.y 
        # print( "{} {} {} {}", self.startX, self.startY, self.endX, self.endY)self.now_time
        # self.OpticalFlow()
        self.view_class.setcoordinate(self.startX, self.startY, self.endX, self.endY)

    def ctrsirection(self):
        self.view_class.setrollflag()

