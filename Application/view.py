import sys
import os
import tkinter as tk
import cv2
from PIL import Image, ImageTk
import numpy as np
import time
import datetime
import controller
import model
import skvideo.io

class View:
   
    startX = -1
    startY = -1
    endX = -1
    endY = -1 
    threshold = 100
    iteration = 2
    rollflag = 0

    def __init__(self, window, path, config_ini):

        self.path = path

        self.ocr_span = int(config_ini['OCR']['ocrspan'])
        self.delay = int(config_ini['OPENCV']['updatespan'])
        self.ocr_timing = self.ocr_span / self.delay
        self.update_count = 0

        # カメラモジュールの映像を表示するキャンバスを用意する
        self.window = window
        self.window.title("Let's OCR")

        videoname = config_ini['FILE']['videoname']
        if videoname != "":
            moviepath = path + videoname
            self.vcap = cv2.VideoCapture(moviepath)
        else:
            self.vcap = cv2.VideoCapture(0)
        if not self.vcap.isOpened():
            sys.exit()
        self.width = self.vcap.get(cv2.CAP_PROP_FRAME_WIDTH)
        self.height = self.vcap.get(cv2.CAP_PROP_FRAME_HEIGHT)
        if(self.height == 1080): self.height = 800 
        
        self.canvas = tk.Canvas(window, width=self.width, height=self.height)
        self.canvas.pack()

        controller.Controller(self, self.window, self.vcap, self.canvas)

        # 2つめのウィンドウ
        self.frame2 = tk.Toplevel()
        self.canvas2 = tk.Canvas(self.frame2)
        self.canvas2.pack()

        self.model_class = model.Model(self.canvas2, self.path, config_ini)

        # self.delay = 50
        self.update()
        self.window.mainloop()

    # キャンバスに表示されているカメラモジュールの映像を
    # 10ミリ秒ごとに更新する
    def update(self):

        grabbed, frame = self.vcap.read()
        if not grabbed:
            self.vcap.set(cv2.CAP_PROP_POS_FRAMES, 0)
            grabbed, frame = self.vcap.read()
            self.model_class.Resettime()
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        if (self.rollflag == 1):
            frame = cv2.rotate(frame, cv2.ROTATE_180)
        # frame = next(self.vcap)
        # frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
        self.photo = ImageTk.PhotoImage(image = Image.fromarray(frame))
        self.canvas.create_image(0, 0, image = self.photo, anchor = tk.NW, tags = "main")
        self.window.after(self.delay, self.update)
        # 四角形を書く
        self.canvas.create_rectangle(self.startX, self.startY, self.endX, self.endY)

        if( self.startX > 0 and self.endX - self.startX > 0 and self.startY > 0 and self.endY - self.startY > 0 ) :
            
            display = frame[self.startY:self.endY, self.startX:self.endX]

            self.update_count += 1

            display = cv2.cvtColor(display, cv2.COLOR_BGR2GRAY)

            if( self.threshold < 0 ) :
                display = cv2.threshold(display, 255+self.threshold, 255, cv2.THRESH_BINARY)[1]
                display = cv2.bitwise_not(display)
            else :
                display = cv2.threshold(display, self.threshold, 255, cv2.THRESH_BINARY)[1]
            # モルフォロジー変換
            
            kernel = np.ones((5, 5), np.uint8)
            # display = cv2.morphologyEx(display, cv2.MORPH_OPEN, kernel)
            display = cv2.erode(display, kernel, iterations=self.iteration)
            # _w = self.endX - self.startX 
            # _h = self.endY - self.startY 
            # center = (int(_w/2), int(_h/2))
            # angle = 1.0
            # scale = 1.05
            # trans = cv2.getRotationMatrix2D(center, angle , scale)
            # display = cv2.warpAffine(display, trans, (int(_w),int(_h)))
            self.photo2 = ImageTk.PhotoImage(image = Image.fromarray(display))
            self.canvas2.create_image(0, 0, image = self.photo2, anchor = tk.NW, tags = "sub")
            self.canvas2.config(width = abs(self.endX - self.startX))
            self.canvas2.config(height = abs(self.endY - self.startY))
            # print(self.ocr_timing)
            # self.now_time = datetime.datetime.fromtimestamp(time.time())
            if(self.update_count >= self.ocr_timing) :
                self.model_class.ProcessImageSSOCR(display)
                self.update_count = 0


    def destructor(self):
        self.window.destroy()
        self.vcap.release()
        sys.exit()

    def setthreshold(self, threshold):
        self.threshold = threshold
        # print(self.threshold)

    def setiteration(self, iteration):
        self.iteration = iteration
        # print(self.threshold)

    def setcoordinate(self, sx, sy, ex, ey):
        self.startX = sx
        self.startY = sy
        self.endX = ex
        self.endY = ey

    def setrollflag(self):
        self.rollflag = 1- self.rollflag 

