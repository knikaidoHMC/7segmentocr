import sys
sys.path.append('/home/knikaido/.local/lib/python3.6/site-packages')
sys.path.append('/usr/lib/python3/dist-packages')
sys.path.append('/usr/local/lib/python3.6/dist-packages')
import tkinter as tk
from PIL import Image, ImageTk
import view
import os
import configparser

class App:
   
    def __init__(self, path):

        window = tk.Tk()

        # configから読み出し
        configpath = path + 'config.ini'
        config_ini = configparser.ConfigParser()
        config_ini.read(configpath, encoding='utf-8')

        view_class = view.View(window, path, config_ini)
        view_class.update()
        window.mainloop()

if __name__ == "__main__":

    # 自分の環境に応じて変える
    path = os.getcwd() + '/Application/'
    print(path)

    App(path)