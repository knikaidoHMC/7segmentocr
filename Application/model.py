import sys
import os
import cv2
from PIL import Image, ImageTk
import numpy as np
import pyocr
import pyocr.builders
import time
import datetime
import pandas as pd
import re
import ssocr

class Model:

    count = 0

    def __init__(self, canvas2, path, config_ini):

        #OCRの準備
        tools = pyocr.get_available_tools()
        if len(tools) == 0:
            print("No OCR tool found")
            sys.exit(1)
        tool = tools[0]
        self.ocrtool = tool
        print("Will use tool '%s'" % (tool.get_name()))
        
        self.pattern = r'(\d+)(?:\.\d+)?'
        self.ssocr_class = ssocr.SSOCR()

        # langs = tool.get_available_languages()
        # print("Available languages: %s" % ", ".join(langs))
        # lang = langs[0]
        # print("Will use lang '%s'" % (lang))

        self.path = path

        date_format = '%Y-%m-%d %H:%M:%S'        
        start_date_str = config_ini['DATE']['start']
        self.flow_start_date = datetime.datetime.strptime(start_date_str, date_format)
        print(self.flow_start_date)
        self.prog_start_date = datetime.datetime.fromtimestamp(time.time())

        self.iscsv = config_ini['OCR']['csv']
        if( self.iscsv == 'true' ):
            self.csvname = config_ini['FILE']['flowname']
            self.df_time = pd.DataFrame(columns=['time', 'flow(m/s)'])
            self.list_time = []
            self.list_txt = []

    def ProcessImageSSOCR(self, display):
        self.now_time = datetime.datetime.fromtimestamp(time.time())
        digits = self.ssocr_class.ssocr(display)
        digits = ''.join(digits)
        print('origin str: ' + digits)
        if(re.fullmatch(self.pattern, digits) != None):
            ocr_flow = (float)(digits)
            rap_time = self.now_time - self.prog_start_date + self.flow_start_date
            print('time:', rap_time)
            print('flow:', ocr_flow)
            if( self.iscsv == 'true' ):
                # self.list_time += [rap_time]
                # self.list_txt += [rap_time]
                tmp_se = pd.Series( [ rap_time, ocr_flow ], index=self.df_time.columns )
                self.df_time = self.df_time.append( tmp_se, ignore_index=True )
                # self.df_time['time'] = self.list_time
                self.count += 1
                # ここに入れちゃうとちょっと処理遅くなっちゃうので，closeとかに入れたい
                csvpath = self.path + 'flow.csv'
                self.df_time.to_csv(csvpath)

    def ProcessImage(self, display):
        # self.RecognizeSSOCR(display)
        self.iplimage = self.cv2pil(display) #上述の画像処理後の画像データ
        self.Recognize(self.iplimage, self.ocrtool)

    def Recognize(self, iplimage, tool):
        #OCR実行
        self.now_time = datetime.datetime.fromtimestamp(time.time())
        txt = tool.image_to_string(
            iplimage,
            lang="letsgodigital",
            builder=pyocr.builders.LineBoxBuilder(tesseract_layout=6)
        )
        if( self.Judgenum(txt) ):
            ocr_flow = (float)(txt[0].content)
            rap_time = self.now_time - self.prog_start_date + self.flow_start_date
            print('time:', rap_time)
            print('flow:', ocr_flow)
            print('confidence:', txt[0].word_boxes[0].confidence)
            if( self.iscsv == 'true' ):
                tmp_se = pd.Series( [ rap_time, ocr_flow ], index=self.df_time.columns )
                self.df_time = self.df_time.append( tmp_se, ignore_index=True )
                self.count += 1
                # ここに入れちゃうとちょっと処理遅くなっちゃうので，closeとかに入れたい
                csvpath = self.path + self.csvname
                self.df_time.to_csv(csvpath)
                
    #OpenCV型→PIL型に変換
    def cv2pil(self, image):
        ''' OpenCV型 -> PIL型 '''
        new_image = image.copy()
        if new_image.ndim == 2:  # モノクロ
            pass
        elif new_image.shape[2] == 3:  # カラー
            new_image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        elif new_image.shape[2] == 4:  # 透過
            new_image = cv2.cvtColor(image, cv2.COLOR_BGRA2RGBA)
        new_image = Image.fromarray(new_image)
        return new_image

    def Judgenum(self, _str):
        if(len(_str) <= 0): return False
        print('origin str: ' + _str[0].content)
        if(re.fullmatch(self.pattern, _str[0].content) == None): return False

        return True

    def Resettime(self):
        self.prog_start_date = self.now_time
